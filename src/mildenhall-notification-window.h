/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MILDENHALL_NOTIFICATION_WINDOW_H__
#define __MILDENHALL_NOTIFICATION_WINDOW_H__

#include <glib-object.h>
#include <clutter/clutter.h>
#include "mildenhall-notification.h"

G_BEGIN_DECLS

#define MILDENHALL_TYPE_NOTIFICATION_WINDOW (mildenhall_notification_window_get_type ())
G_DECLARE_FINAL_TYPE (MildenhallNotificationWindow, mildenhall_notification_window, MILDENHALL,
                      NOTIFICATION_WINDOW, ClutterActor)
/**
 * MildenhallNotificationState:
 * @MILDENHALL_NOTIFICATION_NONE: No notifications are visible.
 *
 * @MILDENHALL_NOTIFICATION_SHOWING: The animation make a notification appear
 *  is in progress (transition from @MILDENHALL_NOTIFICATION_NONE to
 *   @MILDENHALL_NOTIFICATION_SHOWN).
 *
 * @MILDENHALL_NOTIFICATION_SHOWN: A notification is fully visible.
 *
 * @MILDENHALL_NOTIFICATION_HIDING: The animation make a notification disappear
 *  is in progress (transition from @MILDENHALL_NOTIFICATION_SHOWN to
 *   @MILDENHALL_NOTIFICATION_NONE).
 *
 */
typedef enum
{
  MILDENHALL_NOTIFICATION_NONE = 0,
  MILDENHALL_NOTIFICATION_SHOWING,
  MILDENHALL_NOTIFICATION_SHOWN,
  MILDENHALL_NOTIFICATION_HIDING,
} MildenhallNotificationState;

/**
 * MildenhallNotificationClosedReason:
 * @MILDENHALL_NOTIFICATION_CLOSED_EXPIRED: Notification expired.
 *
 * @MILDENHALL_NOTIFICATION_CLOSED_BY_USER: Notification explicitly closed by user.
 *
 * @MILDENHALL_NOTIFICATION_CLOSED_CLOSE_NOTIFICATION: The notification was closed by
 *  a call to CloseNotification.
 *
 * @MILDENHALL_NOTIFICATION_CLOSED_UNKNOWN: Undefined/reserved reasons.
 */
typedef enum
{
  MILDENHALL_NOTIFICATION_CLOSED_EXPIRED = 1,
  MILDENHALL_NOTIFICATION_CLOSED_BY_USER,
  MILDENHALL_NOTIFICATION_CLOSED_CLOSE_NOTIFICATION,
  MILDENHALL_NOTIFICATION_CLOSED_UNKNOWN,
} MildenhallNotificationClosedReason;

typedef enum
{
  MILDENHALL_NOTIFICATION_INVALID_NUMBER_OF_ACTIONS,
  MILDENHALL_NOTIFICATION_INVALID_NOTIFICATION,
  MILDENHALL_NOTIFICATION_ACCESS_DENIED,
  MILDENHALL_NOTIFICATION_UNKNOWN_ERROR,
} MildenhallNotificationError;

GQuark mildenhall_notification_window_error_quark (void);

#define MILDENHALL_NOTIFICATION_WINDOW_ERROR mildenhall_notification_window_error_quark()

MildenhallNotificationWindow * mildenhall_notification_window_new (MildenhallNotification *notification, GError **error);
void mildenhall_notification_window_show (MildenhallNotificationWindow *self);
void mildenhall_notification_window_hide (MildenhallNotificationWindow *self, MildenhallNotificationClosedReason reason);
MildenhallNotificationClosedReason mildenhall_notification_window_get_notification_closed_reason (MildenhallNotificationWindow *self);
guint mildenhall_notification_window_get_id (MildenhallNotificationWindow *self);
MildenhallNotificationState mildenhall_notification_window_get_state (MildenhallNotificationWindow *self);

G_END_DECLS

#endif /* __MILDENHALL_NOTIFICATION_WINDOW_H__ */
