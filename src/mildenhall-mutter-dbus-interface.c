/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-mutter-plugin.h"
#include "canterbury_app_handler.h"

extern guint  mildenhall_plugin_debug_flags;
guint splash_event_source_id = 0;

static gboolean handle_start_idle_animation_request( CanterburyMutterPlugin *object,
                                                     GDBusMethodInvocation   *invocation,
                                                     gpointer                 user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("handle_start_idle_animation_request \n");
  canterbury_mutter_plugin_complete_start_idle_animation(object , invocation);
  v_idle_animation_start(priv->idle_animation);
  return TRUE;
}

static gboolean handle_stop_idle_animation_request( CanterburyMutterPlugin *object,
													GDBusMethodInvocation   *invocation,
													gpointer                 user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("canterbury_mutter_plugin_complete_stop_idle_animation \n");
  canterbury_mutter_plugin_complete_stop_idle_animation(object , invocation);
  v_idle_animation_stop(priv->idle_animation);
  return TRUE;
}

static gboolean handle_maximize_application( CanterburyMutterPlugin *object,
                                         GDBusMethodInvocation   *invocation,
                                         const gchar* appid,
                                         gpointer user_data)
{
  MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (user_data));
  MetaWindow *window_found = NULL;
  gchar *dot = NULL;
  GList *actors, *l;

  /* Only maximize normal application windows */
  actors = meta_get_window_actors (display);
  mildenhall_plugin_debug("handle maximize for %s\n", appid);

  /* Dig up the window that was requested and maximize it */
  for (l = actors; l; l = g_list_next (l))
    {
      MetaWindow *w = meta_window_actor_get_meta_window (l->data);

      if (!g_strcmp0 (meta_window_get_title (w), appid))
        {
          window_found = w;
          break;
        }
    }

  if (!window_found)
    dot = g_strrstr_len (appid, -1, ".");

  /* If there is no window with the exact title, try to match the first
   * part of the name. For example, if org.apertis.Framton.Artists
   * was not found, search for org.apertis.Frampton.*, such as
   * org.apertis.Frampton.Albums
   * https://phabricator.apertis.org/T4069 will replace this logic. */
  for (l = actors; dot && l; l = g_list_next (l))
    {
      MetaWindow *w = meta_window_actor_get_meta_window (l->data);

      if (g_ascii_strncasecmp (meta_window_get_title (w),
                               appid, dot - appid) == 0)
        {
          window_found = w;
          break;
        }
    }

  if (window_found)
     meta_window_unminimize (window_found);
  else
     WARNING("Couldn't maximize %s (suffix %s)\n", appid, dot);

  return TRUE;
}

static gboolean handle_minimize_request( CanterburyMutterPlugin *object,
                                         GDBusMethodInvocation   *invocation,
                                         const gchar*       launched_client,
                                         gpointer                 user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("handle_minimize_request \n");

  if( priv->prev_win_apriv->actor != NULL )
    meta_window_minimize (priv->prev_win_apriv->meta_window);

  if(priv->splash_screen)
  {
    clutter_actor_remove_child(priv->app_container , priv->splash_screen);
    clutter_actor_destroy(priv->splash_screen);
    priv->splash_screen = NULL;
    clutter_actor_hide(priv->app_placeholder);
  }
  canterbury_mutter_plugin_complete_minimize_current_application(object , invocation);
  return TRUE;
}
static gboolean handle_display_off( CanterburyMutterPlugin *object,
                                    GDBusMethodInvocation   *invocation,
                                    gboolean animate,
                                    gpointer                 user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("handle_display_off \n");
  priv->shutdown = TRUE;
  clutter_actor_show(priv->display_off);
  if(animate)
   v_idle_animation_start(priv->idle_animation);

  canterbury_mutter_plugin_complete_display_off(object , invocation);
  return TRUE;
}

static gboolean handle_hide_window( CanterburyMutterPlugin *object,
                                    GDBusMethodInvocation   *invocation,
                                    gchar*  pWindowName,
                                    gpointer  user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("handle_hide_window \n");
  priv->win_name_to_hide = g_strdup(pWindowName);
  canterbury_mutter_plugin_complete_hide_window_when_shown(object , invocation);
  return TRUE;
}

static gboolean hide_splash_screen(gpointer userdata)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (userdata)->priv;
  if(priv->splash_screen)
  {
    clutter_actor_remove_child(priv->app_container , priv->splash_screen);
    clutter_actor_destroy(priv->splash_screen);
    priv->splash_screen = NULL;
    clutter_actor_hide(priv->app_placeholder);
  }
  return FALSE;
}

static gboolean handle_hide_splash_screen( CanterburyMutterPlugin *object,
                                           GDBusMethodInvocation   *invocation,
                                           gpointer  user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("handle_hide_splash_screen \n");
  if(priv->splash_screen)
  {
    splash_event_source_id = g_timeout_add_seconds(6.5, hide_splash_screen, user_data);
  }
  canterbury_mutter_plugin_complete_hide_splash_screen(object , invocation);
  return TRUE;
}

static gboolean handle_app_switch_request( CanterburyMutterPlugin *object,
                                           GDBusMethodInvocation  *invocation,
                                           const gchar *app_type,
                                           const gchar *arg_launched_client,
                                           guint arg_launch_args,
                                           const gchar *splash_screen,
                                           gpointer user_data)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;
  if(priv->launcher_apriv == NULL)
  {
    mildenhall_plugin_debug("handle_app_switch_request -> launcher not ready \n");
    canterbury_mutter_plugin_complete_start_app_switch(object , invocation);
    return TRUE;
  }

  mildenhall_plugin_debug("handle_app_switch_request launcher stats %d %d %s \n" , priv->launcher_apriv->is_maximized , priv->launcher_apriv->is_minimized , app_type );
  priv->app_type = g_strdup(app_type);
  if (priv->launcher_apriv->is_maximized != priv->launcher_apriv->is_minimized)
  {
	  mildenhall_plugin_debug("launcher is in ");
	  if(priv->splash_screen)
	  {
      clutter_actor_remove_child(priv->app_container , priv->splash_screen);
	    clutter_actor_destroy(priv->splash_screen);
	    priv->splash_screen = NULL;
	  }

	  if (g_file_test(splash_screen , G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
	  {
      priv->splash_screen = p_ui_texture_from_file (splash_screen);
      mildenhall_plugin_debug("splash_screen %s \n ", splash_screen);
	  }
	  if(( priv->launcher_apriv->is_maximized == TRUE ) && ( priv->launcher_apriv->is_minimized == FALSE ))
	  {
		  mildenhall_plugin_debug("maximized state \n");
		  start_swipe_in_animation(user_data);
	  }
	  else
	  {
      gboolean back_animation = FALSE;
      if( 0 == g_strcmp0(arg_launched_client, CANTERBURY_BACK_KEY) )
      {
        back_animation = TRUE;
      }
		  mildenhall_plugin_debug("minimized state %s \n", arg_launched_client);
		  start_up_down_animation(user_data, back_animation); /* Parameter animate is to check the back click for reverse animation*/
	  }
  }

  canterbury_mutter_plugin_complete_start_app_switch(object , invocation);
  return TRUE;
}

void
on_bus_acquired (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
  MetaPlugin *plugin = user_data;
  MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;

  mildenhall_plugin_debug("on_bus_acquired %s \n" , name);
  /* here we export all the methods supported by the service, this callback is called before on_name_acquired*/
  /* once on_name_acquired is called clients can start calling service interfaces, so its important to export all methods here */

  /* Creates a new skeleton object, ready to be exported */
  priv->mutter_dbus_object = canterbury_mutter_plugin_skeleton_new ();

  /* provide signal handlers for all methods */
  g_signal_connect (priv->mutter_dbus_object,
                    "handle-start-idle-animation",
                    G_CALLBACK (handle_start_idle_animation_request),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-stop-idle-animation",
                    G_CALLBACK (handle_stop_idle_animation_request),
                    user_data);


  g_signal_connect (priv->mutter_dbus_object,
                    "handle-start-app-switch",
                    G_CALLBACK (handle_app_switch_request),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-maximize-application",
                    G_CALLBACK (handle_maximize_application),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-minimize-current-application",
                    G_CALLBACK (handle_minimize_request),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-display-off",
                    G_CALLBACK (handle_display_off),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-hide-window-when-shown",
                    G_CALLBACK (handle_hide_window),
                    user_data);

  g_signal_connect (priv->mutter_dbus_object,
                    "handle-hide-splash-screen",
                    G_CALLBACK (handle_hide_splash_screen),
                    user_data);

   /* Exports interface launcher_object at object_path "/canterbury/Mutter/Plugin" on connection */
  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (priv->mutter_dbus_object),
                                         connection,
                                         "/org/apertis/Canterbury/Mutter/Plugin",
                                         NULL))
  {
    g_printerr("export error \n");
  }

}

void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{

	mildenhall_plugin_debug("on_name_acquired %s \n" , name);
}

void
on_name_lost (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{
  /* free all dbus handlers ... */
	mildenhall_plugin_debug("on_name_lost \n");
}
