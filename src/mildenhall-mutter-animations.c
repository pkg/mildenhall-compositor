/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-mutter-plugin.h"

extern guint  mildenhall_plugin_debug_flags;
guint event_source_id = 0;
guint placeholder_event_source_id = 0;
guint launcher_event_source_id = 0;
extern guint back_button_source_id;

static gboolean hide_placeholder (gpointer userdata);

gboolean hide_back_button(gpointer userdata)
{
  MetaPlugin *plugin = userdata;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
  clutter_actor_hide(priv->back_button);
  priv->swipe_in_animate = FALSE;
  priv->launcher_max_animate = FALSE;

  return FALSE;
}

static void
window_destroyed_cb (ClutterActor *actor,
                     gpointer      user_data)
{
  EffectCompleteData *launcher_data = user_data;

  /* The actor (window) has been destroyed externally while we were waiting for
   * transitions to complete on it. Clear our pointer to it, which
   * on_launcher_min_effect_complete() will use as a signal to bail. */
  g_assert (launcher_data->actor == actor);
  launcher_data->actor = NULL;
}

void start_launcher_maximize_animation( MetaPlugin *plugin )
{
    MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
    EffectCompleteData *launcher_data  = g_new0 (EffectCompleteData, 1);
    priv->launcher_max_animate = TRUE;
    clutter_actor_save_easing_state(priv->launcher_apriv->actor);
    clutter_actor_set_easing_mode (priv->launcher_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
    clutter_actor_set_easing_duration (priv->launcher_apriv->actor, MAP_TIMEOUT);
    clutter_actor_set_x (priv->launcher_apriv->actor, 0);
    clutter_actor_restore_easing_state (priv->launcher_apriv->actor);
    clutter_actor_show(priv->back_button);
    clutter_actor_hide(priv->app_placeholder);
    launcher_data->plugin = plugin;
    launcher_data->actor = priv->launcher_apriv->actor;

    launcher_data->destroy_id = g_signal_connect (launcher_data->actor, "destroy",
                                                  (GCallback) window_destroyed_cb,
                                                  launcher_data);

    priv->launcher_apriv->is_minimized = FALSE;
    launcher_event_source_id = g_signal_connect ( priv->launcher_apriv->actor,
                                                  "transitions-completed",
                                                  G_CALLBACK (on_launcher_max_effect_complete),
                                                  launcher_data);
}

static void
on_swipe_effect_complete (ClutterActor *actor,
			 gpointer userdata)
{
  MetaPlugin *plugin = userdata;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
  g_signal_emit_by_name (priv->MildenhallPluginObj, "app-switch-complete");
  priv->app_switch_complete = TRUE;
  mildenhall_plugin_debug("on_swipe_effect_complete  \n");
  g_signal_handler_disconnect(actor , event_source_id);

  /* Was the window destroyed during the transition? */
  if (!priv->map_complete && priv->data->actor == NULL)
    {
      /* FIXME: Do we want to undo the transition so far? */
      return;
    }

  if (priv->data && priv->data->actor)
      clutter_actor_set_y (priv->data->actor, 0);

  if(priv->map_complete == FALSE)
  {
	  mildenhall_plugin_debug("not yet mapped \n");
	  show_new_window(priv->data);
  }
	//priv->swipe_in_animate = FALSE;
}



static void
on_launcher_min_effect_complete (ClutterActor *actor,
							gpointer userdata)
{
  EffectCompleteData *data = userdata ;
  MetaPlugin *plugin = data->plugin;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
  MetaWindowActor  *window_actor = META_WINDOW_ACTOR (data->actor);
  MetaWindow *meta_window = NULL;

  /* Was the window destroyed before the transition completed? */
  if (data->actor == NULL)
    {
      start_launcher_maximize_animation (plugin);
      effect_complete_data_free (data);
      return;
    }

  canterbury_mutter_plugin_emit_launcher_state(priv->mutter_dbus_object, 3);
  g_timeout_add(500, hide_back_button, data->plugin);

  priv->launcher_apriv->is_minimized = TRUE;
  g_signal_handler_disconnect(actor , launcher_event_source_id);
  meta_window = meta_window_actor_get_meta_window (window_actor);
  /* set the actual position of the window */
  //meta_window_move( meta_window,TRUE,-810,0);
	meta_window_move_resize_frame (meta_window,
                                       TRUE,
                                       LAUNCHER_OUTSIDE_SCREEN_ORIGIN + priv->bezel_offset_x,
                                       priv->bezel_offset_y,
                                       LAUNCHER_WINDOW_WIDTH,
                                       ASSUMED_SCREEN_HEIGHT);
  effect_complete_data_free (data);

  //priv->swipe_in_animate = FALSE;
}

void
on_launcher_max_effect_complete (ClutterActor *actor,
                                 gpointer userdata)
{
  EffectCompleteData *data = userdata ;
  MetaPlugin *plugin = data->plugin;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
  MetaWindowActor  *window_actor = META_WINDOW_ACTOR (data->actor);
  MetaWindow *meta_window = NULL;

  /* Was the window destroyed before the transition completed? */
  if (data->actor == NULL)
    {
      effect_complete_data_free (data);
      return;
    }

  g_signal_handler_disconnect(actor , launcher_event_source_id);
  priv->launcher_apriv->is_maximized = TRUE;
  mildenhall_plugin_debug("on_launcher_max_effect_complete \n");
  meta_window = meta_window_actor_get_meta_window (window_actor);
  /* set the actual position of the window */
  meta_window_move_resize_frame (meta_window,TRUE, 0,0, LAUNCHER_WINDOW_WIDTH, ASSUMED_SCREEN_HEIGHT);
  canterbury_mutter_plugin_emit_app_switch_completed(priv->mutter_dbus_object);
  canterbury_mutter_plugin_emit_launcher_state(priv->mutter_dbus_object, 1);
  g_timeout_add(500, hide_back_button, data->plugin);

  effect_complete_data_free (data);

	//priv->launcher_max_animate = FALSE;
}

static gboolean 
hide_placeholder (gpointer userdata)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (userdata)->priv;


  ClutterTransition *transition;
  transition = clutter_property_transition_new ("opacity");
  clutter_timeline_set_duration (CLUTTER_TIMELINE (transition), 1000);
  clutter_transition_set_from (transition, G_TYPE_UINT, 255);
  mildenhall_plugin_debug("hide_placeholder \n ");
  if(priv->splash_screen)
  {
    clutter_transition_set_to (transition, G_TYPE_UINT, 200);
  }
  else
  {
    clutter_transition_set_to (transition, G_TYPE_UINT, 0);
  }
  clutter_actor_add_transition (priv->app_placeholder, "animate-opacity", transition);
  canterbury_mutter_plugin_emit_app_switch_completed(priv->mutter_dbus_object);

  return FALSE;
}

void
show_new_window (EffectCompleteData *data )
{

  MetaPlugin *plugin = data->plugin;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
  MetaWindowActor  *window_actor = META_WINDOW_ACTOR (data->actor);
  ActorPrivate *apriv;
  MetaWindow *meta_window;
  guint position_x, position_y, width, height;

  /* The caller must handle the case where the actor was destroyed before the
   * transition (or whatever) was completed. */
  g_assert (META_IS_WINDOW_ACTOR (data->actor));

  apriv = get_actor_private (window_actor);
  meta_window = meta_window_actor_get_meta_window (window_actor);
  meta_window_raise(priv->launcher_apriv->meta_window);
  priv->map_complete = TRUE;
  priv->app_switch_complete = FALSE;
  apriv->tml_map = NULL;
  mildenhall_plugin_debug("show_new_window width %f height %f\n" , clutter_actor_get_width(data->actor) , clutter_actor_get_height(data->actor));

  clutter_actor_set_pivot_point(data->actor , 0 , 0);
  if( FALSE == g_strcmp0(priv->app_type , "EXT_APPLICATION") )
  {
    position_x = MINI_LAUNCHER_WIDTH + priv->bezel_offset_x;
    position_y = STATUS_BAR_HEIGHT + priv->bezel_offset_y;
    width = APPLICATION_WIDTH;
    height = APPLICATION_HEIGHT;
  }
  else
  {
    position_x = MINI_LAUNCHER_WIDTH + priv->bezel_offset_x;
    position_y = 0 + priv->bezel_offset_y;
    width = APPLICATION_WIDTH;
    height = ASSUMED_SCREEN_HEIGHT;
  }
  clutter_actor_set_position (data->actor, position_x, position_y);
  clutter_actor_show (data->actor);
  /* Now notify the manager that we are done with this effect */
  if(priv->splash_screen)
  {
    mildenhall_plugin_debug("show_splash screen \n ");
    clutter_actor_set_position (priv->splash_screen, 130, 100);
    clutter_actor_set_size (priv->splash_screen, 600, 300);
    clutter_actor_insert_child_above(priv->app_container , priv->splash_screen , priv->app_placeholder );
  }

  /* set the actual position of the window */
  meta_window_move_resize_frame (meta_window,TRUE, position_x,position_y,width , height);
  priv->prev_win_apriv = get_actor_private (window_actor);
  priv->prev_win_apriv->is_maximized = TRUE;
  priv->prev_win_apriv->is_minimized = FALSE;
  priv->prev_win_apriv->actor = data->actor;
  priv->prev_win_apriv->meta_window = meta_window;
  /* delay increased to avoid duplicate mini launcher. Need a proper way to know
     when to hide the placeholder  */
  placeholder_event_source_id = g_timeout_add(2500, hide_placeholder, plugin);
}

void
start_swipe_in_animation( MetaPlugin *plugin )
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;

  EffectCompleteData *launcher_data  = g_new0 (EffectCompleteData, 1);

  /* swipe in the new app window  */
  if(placeholder_event_source_id)
  {
	  g_source_remove(placeholder_event_source_id);
  }
  priv->swipe_in_animate = TRUE;
  clutter_actor_remove_transition(priv->app_placeholder,"animate-opacity");
  clutter_actor_set_opacity (priv->app_placeholder, 255);
  clutter_actor_show(priv->app_placeholder);
  clutter_actor_set_position (priv->app_placeholder, LAUNCHER_WINDOW_WIDTH, STATUS_BAR_HEIGHT);
  clutter_actor_save_easing_state(priv->app_placeholder);
  clutter_actor_set_easing_mode (priv->app_placeholder, CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (priv->app_placeholder, MAP_TIMEOUT);
  clutter_actor_set_x (priv->app_placeholder, MINI_LAUNCHER_WIDTH);
  clutter_actor_restore_easing_state (priv->app_placeholder);

  if(back_button_source_id)
    g_source_remove(back_button_source_id);
  clutter_actor_show(priv->back_button);

  canterbury_mutter_plugin_emit_launcher_state(priv->mutter_dbus_object, 2);

  event_source_id = g_signal_connect (priv->app_placeholder,
                    "transitions-completed",
                    G_CALLBACK (on_swipe_effect_complete),
                    plugin);

  /* swipe out the launcher window  */
   clutter_actor_save_easing_state(priv->launcher_apriv->actor);
   clutter_actor_set_easing_mode (priv->launcher_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
   clutter_actor_set_easing_duration (priv->launcher_apriv->actor, MAP_TIMEOUT);
   clutter_actor_set_x (priv->launcher_apriv->actor, LAUNCHER_OUTSIDE_SCREEN_ORIGIN);
   clutter_actor_restore_easing_state (priv->launcher_apriv->actor);

   launcher_data->plugin = plugin;
   launcher_data->actor = priv->launcher_apriv->actor;

   launcher_data->destroy_id = g_signal_connect (launcher_data->actor, "destroy",
                                                 (GCallback) window_destroyed_cb,
                                                 launcher_data);

   priv->launcher_apriv->is_maximized = FALSE;

   launcher_event_source_id = g_signal_connect (priv->launcher_apriv->actor,
                                                 "transitions-completed",
                                                 G_CALLBACK (on_launcher_min_effect_complete),
                                                 launcher_data);

}

static void
on_swipe_out_effect_complete (ClutterActor *actor,
                              gpointer userdata)
{
  MetaPlugin *plugin = userdata;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;

  mildenhall_plugin_debug("on_swipe_out_effect_complete \n");


  clutter_actor_hide(actor);

  meta_plugin_minimize_completed (plugin,
    priv->prev_win_apriv->meta_window_actor);
}

void
start_app_swipe_out_animation( MetaPlugin *plugin  , ClutterActor* actor )
{
	MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
	if(actor != priv->prev_win_apriv->actor)
	{
		return;
	}
	if(placeholder_event_source_id)
	{
		g_source_remove(placeholder_event_source_id);
	}

  clutter_actor_remove_transition(priv->app_placeholder,"animate-opacity");
  clutter_actor_hide(priv->app_placeholder);

  clutter_actor_save_easing_state(actor);
  clutter_actor_set_easing_mode (actor, CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (actor, MAP_TIMEOUT);
  clutter_actor_set_x (actor, 800);
  clutter_actor_restore_easing_state (actor);

  g_signal_connect ( actor,
                     "transitions-completed",
                     G_CALLBACK (on_swipe_out_effect_complete),
                     plugin);

  canterbury_mutter_plugin_emit_launcher_state(priv->mutter_dbus_object, 2);
  start_launcher_maximize_animation(plugin);
}



static void
on_pull_up_effect_complete (ClutterActor *actor,
			    gpointer userdata)
{

  mildenhall_plugin_debug("pull up complete \n");
  g_signal_handler_disconnect(actor , event_source_id);
  hide_placeholder (userdata);
}

static void
on_pull_out_old_win_effect_complete (ClutterActor *actor,
				     gpointer userdata)
{
  MetaPlugin *plugin = userdata;
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;

  mildenhall_plugin_debug("on_pull_out_old_win_effect_complete \n");

  g_signal_emit_by_name (priv->MildenhallPluginObj, "app-switch-complete");
  priv->app_switch_complete = TRUE;

  /* Was the actor destroyed while waiting for the transition? */
  if (!priv->map_complete && priv->data->actor == NULL)
    {
      /* FIXME: Do we want to undo the transition so far? */
      return;
    }

  if (priv->data && priv->data->actor)
      clutter_actor_set_y (priv->data->actor, 0);

  if(priv->map_complete == FALSE)
  {
	  mildenhall_plugin_debug("not yet mapped \n");
	  show_new_window(priv->data);
  }
}

void start_up_down_animation( MetaPlugin *plugin, gboolean back_press )
{
	  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
	  int y_position = -430, position = 480;

	  if(back_press == TRUE) /* Reverse Animation for back press */
	  {
		  y_position = 480;
		  position = -380;
	  }
	  mildenhall_plugin_debug("start_pull_up_animation \n");

	  /*pull out the old app window  */
	  if( (priv->prev_win_apriv) && (priv->prev_win_apriv->is_maximized) )
	  {
		  mildenhall_plugin_debug("pull out old window \n");
		  clutter_actor_save_easing_state(priv->prev_win_apriv->actor);
		  clutter_actor_set_easing_mode (priv->prev_win_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
		  clutter_actor_set_easing_duration (priv->prev_win_apriv->actor, MAP_TIMEOUT);
		  clutter_actor_set_y (priv->prev_win_apriv->actor, y_position);
		  clutter_actor_restore_easing_state (priv->prev_win_apriv->actor);

		  g_signal_connect (priv->prev_win_apriv->actor,
		                   "transitions-completed",
		                    G_CALLBACK (on_pull_out_old_win_effect_complete),
		                    plugin);
	  }

	  /*pull up or down the new app window  */
	  if(placeholder_event_source_id)
     g_source_remove(placeholder_event_source_id);

    clutter_actor_remove_transition(priv->app_placeholder,"animate-opacity");
    clutter_actor_set_opacity (priv->app_placeholder, 255);
	  clutter_actor_show(priv->app_placeholder);
	  clutter_actor_set_position (priv->app_placeholder, MINI_LAUNCHER_WIDTH, position);
	  clutter_actor_save_easing_state(priv->app_placeholder);
	  clutter_actor_set_easing_mode (priv->app_placeholder, CLUTTER_EASE_OUT_CUBIC);
	  clutter_actor_set_easing_duration (priv->app_placeholder, MAP_TIMEOUT);
	  clutter_actor_set_y (priv->app_placeholder, STATUS_BAR_HEIGHT);
	  clutter_actor_restore_easing_state (priv->app_placeholder);

	  event_source_id = g_signal_connect (priv->app_placeholder,
	                    "transitions-completed",
	                    G_CALLBACK (on_pull_up_effect_complete),
	                    plugin);
}
