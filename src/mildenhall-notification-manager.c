/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <meta/meta-plugin.h>
#include <meta/window.h>
#include <meta/theme.h>
#include <meta/display.h>
#include <meta/util.h>

#include "org.freedesktop.Notifications.h"
#include "mildenhall-notification-manager.h"
#include "mildenhall-notification-window.h"

typedef struct _MildenhallNotificationManager
{
  GObject parent;

  GPtrArray *notification_list;
  MetaPlugin *plugin;
  MildenhallNotificationState state;
  guint notification_id;
  OrgFreedesktopNotifications *skeleton_obj;
  MildenhallNotificationWindow *window;
  guint owner_id;
} MildenhallNotificationManager;

enum
{
  PROP_META_PLUGIN = 1,
};

typedef struct
{
  MildenhallNotificationManager *self;
  GDBusMethodInvocation *invocation;
  guint id;
} CloseNotificationData;

G_DEFINE_TYPE (MildenhallNotificationManager, mildenhall_notification_manager, G_TYPE_OBJECT);

static void show_new_notification (MildenhallNotification *notification);

static void
close_notification_data_free (CloseNotificationData *data)
{
  g_object_unref (data->invocation);
  g_object_unref (data->self);
  g_slice_free (CloseNotificationData, data);
}

static void
notify_process_info_cb (GObject *source_object, GAsyncResult *res,
                        gpointer user_data)
{
  MildenhallNotification *notification = user_data;
  MildenhallNotificationManager *self = MILDENHALL_NOTIFICATION_MANAGER (notification->nm_ref);
  GError *error = NULL;
  CbyProcessType process_type;

  notification->process_info = cby_process_info_new_for_dbus_invocation_finish(res, &error);
  if (notification->process_info == NULL)
    {
      g_dbus_method_invocation_take_error (notification->notify_invocation,
                                           error);
      mildenhall_notification_free (notification);
      return;
    }
  process_type = cby_process_info_get_process_type (notification->process_info);

  if (process_type == CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_dbus_method_invocation_return_dbus_error (
          notification->notify_invocation,
          "org.freedesktop.DBus.Error.AccessDenied",
          "Could not determine credentials");
      mildenhall_notification_free (notification);
      return;
    }

  /*TODO: Priority handling has to be done.
   * Based on the priority Add the data in order to the list
   * Support for reading priorities has to be from libcanterbury which is not yet available*/
  g_ptr_array_add (self->notification_list, notification);
  if (self->state == MILDENHALL_NOTIFICATION_NONE)
    {
      show_new_notification (notification);
    }
}

static gboolean
handle_notify_cb (OrgFreedesktopNotifications *object,
                  GDBusMethodInvocation *invocation,
                  const gchar *arg_app_name,
                  guint arg_replaces_id,
                  const gchar *arg_app_icon,
                  const gchar *arg_summary,
                  const gchar *arg_body,
                  const gchar * const *arg_actions,
                  GVariant *arg_hints,
                  gint arg_expire_timeout,
                  gpointer user_data)
{
  /*TODO: Hide any modal dialog box if shown.
   * The modal dialogs which are created in application process
   * should be hidden ,currently creation of modal dialogs by application
   * is not available*/

  MildenhallNotificationManager *self = user_data;
  MildenhallNotification *notification = NULL;

  self->notification_id++;
  notification = mildenhall_notification_new (self, NULL, arg_app_icon,
                                              arg_summary, arg_body,
                                              arg_actions, arg_hints,
                                              self->notification_id,
                                              arg_expire_timeout, invocation,
                                              NULL);

  cby_process_info_new_for_dbus_invocation_async (invocation, NULL,
                                                  notify_process_info_cb,
                                                  notification);
  return TRUE;
}

static MildenhallNotification *
search_notification_from_the_list (MildenhallNotificationManager *self,
                                   CbyProcessInfo *process_info,
                                   guint id)
{
  guint counter = 0;
  MildenhallNotification *notification = NULL;
  const gchar *bundle_id = NULL;
  const gchar *apparmor_label = NULL;

  g_return_val_if_fail (self->notification_list != NULL, NULL);
  g_return_val_if_fail (self->notification_list->len > 0, NULL);

  bundle_id = cby_process_info_get_bundle_id (process_info);
  apparmor_label = cby_process_info_get_apparmor_label (process_info);

  if (bundle_id == NULL && apparmor_label == NULL)
    return NULL;
  for (counter = 0; counter < self->notification_list->len; counter++)
    {
      MildenhallNotification *temp_notification;
      const gchar *notification_apparmor_label;

      temp_notification = g_ptr_array_index (self->notification_list, counter);
      notification_apparmor_label = cby_process_info_get_apparmor_label (
          temp_notification->process_info);
      if (temp_notification->id == id
          && !g_strcmp0 (notification_apparmor_label, apparmor_label))
        {
          notification = temp_notification;
          break;
        }
    }

  return notification;
}

static void
close_notification_process_info_cb (GObject *source_object, GAsyncResult *res,
                                    gpointer user_data)
{
  GTask *task = user_data;
  MildenhallNotification *notification = NULL;
  MildenhallNotificationManager *self;
  CbyProcessInfo *process_info = NULL;
  GError *error = NULL;
  CbyProcessType process_type;
  CloseNotificationData *method_data;

  method_data = g_task_get_task_data (task);
  self = method_data->self;
  process_info = cby_process_info_new_for_dbus_invocation_finish (res, &error);
  if (process_info == NULL)
    {
      g_task_return_error (task, error);
      goto cleanup_and_exit;
    }
  process_type = cby_process_info_get_process_type (process_info);

  if (process_type == CBY_PROCESS_TYPE_UNKNOWN)
    {
      g_task_return_new_error (task, MILDENHALL_NOTIFICATION_WINDOW_ERROR,
                               MILDENHALL_NOTIFICATION_ACCESS_DENIED,
                               "Access denied to close notification");
      goto cleanup_and_exit;
    }

  notification = search_notification_from_the_list (self, process_info,
                                                    method_data->id);
  if (notification == NULL)
    {
      g_task_return_new_error (task, MILDENHALL_NOTIFICATION_WINDOW_ERROR,
                               MILDENHALL_NOTIFICATION_ACCESS_DENIED,
                               "Access Denied or invalid ID to close");
      goto cleanup_and_exit;
    }

  if (self->state == MILDENHALL_NOTIFICATION_SHOWN)
    {
      notification->close_task = g_object_ref (task);
      mildenhall_notification_window_hide (
        self->window, MILDENHALL_NOTIFICATION_CLOSED_CLOSE_NOTIFICATION);
    }
  else
    {
      g_task_return_new_error (task, MILDENHALL_NOTIFICATION_WINDOW_ERROR,
                               MILDENHALL_NOTIFICATION_INVALID_NOTIFICATION,
                               "Invalid notification to close");
    }

cleanup_and_exit:
  g_clear_object (&task);
  g_clear_object (&process_info);
}

static void
close_notification_done_cb (GObject *object,
                            GAsyncResult *result,
                            gpointer user_data)
{
  GDBusMethodInvocation *invocation = user_data;
  GError *error = NULL;

  if (g_task_propagate_boolean (G_TASK (result), &error))
    {
      org_freedesktop_notifications_complete_close_notification (
          (OrgFreedesktopNotifications *) object, invocation);
    }
  else
    g_dbus_method_invocation_return_gerror (invocation, error);

  g_clear_error (&error);
  g_clear_object (&invocation);
}

static gboolean
handle_close_notification_cb (OrgFreedesktopNotifications *object,
                              GDBusMethodInvocation *invocation,
                              guint arg_id,
                              gpointer user_data)
{
  MildenhallNotificationManager *self = user_data;
  CloseNotificationData *method_data;
  GTask *task;

  task = g_task_new (object, NULL, close_notification_done_cb,
                     g_object_ref (invocation));
  method_data = g_slice_new0 (CloseNotificationData);
  method_data->invocation = g_object_ref (invocation);
  method_data->self = g_object_ref (self);
  method_data->id = arg_id;
  g_task_set_task_data (task, method_data,
                        (GDestroyNotify) close_notification_data_free);
  cby_process_info_new_for_dbus_invocation_async (
      invocation, NULL, close_notification_process_info_cb, task);
  return TRUE;
}

static void
on_bus_acquired (GDBusConnection *connection, const gchar *name,
                 gpointer user_data)
{
  MildenhallNotificationManager *self = user_data;
  GError *error = NULL;

  self->skeleton_obj = org_freedesktop_notifications_skeleton_new ();
  g_signal_connect (self->skeleton_obj, "handle-notify",
                    G_CALLBACK (handle_notify_cb), user_data);
  g_signal_connect (self->skeleton_obj, "handle-close-notification",
                    G_CALLBACK (handle_close_notification_cb), user_data);
  if (!g_dbus_interface_skeleton_export (
      G_DBUS_INTERFACE_SKELETON (self->skeleton_obj), connection,
      "/org/freedesktop/Notifications", &error))
    {
      g_warning ("%s: code %d: %s", g_quark_to_string (error->domain),
                 error->code, error->message);
      g_clear_error (&error);
    }
}

static void
on_name_acquired (GDBusConnection *connection, const gchar *name,
                  gpointer user_data)
{

  g_debug ("on_name_acquired %s", name);
}

static void
on_name_lost (GDBusConnection *connection, const gchar *name,
              gpointer user_data)
{
  g_debug ("on_name_lost");
}

static void
notification_window_state_change_cb (GObject *window, GParamSpec *pspec,
                                     gpointer user_data)
{
  MildenhallNotification *notification = user_data;
  MildenhallNotificationManager *self = MILDENHALL_NOTIFICATION_MANAGER (notification->nm_ref);

  self->state = mildenhall_notification_window_get_state (
      (MildenhallNotificationWindow *) window);
  if (self->state == MILDENHALL_NOTIFICATION_SHOWN)
    {
      g_return_if_fail (notification->notify_invocation != NULL);
      org_freedesktop_notifications_complete_notify (
          self->skeleton_obj, notification->notify_invocation,
          notification->id);

    }
  else if (self->state == MILDENHALL_NOTIFICATION_NONE)
    {
      guint previous_notification_id;
      MildenhallNotificationClosedReason reason;

      reason = mildenhall_notification_window_get_notification_closed_reason (
          self->window);
      previous_notification_id = mildenhall_notification_window_get_id (self->window);

      org_freedesktop_notifications_emit_notification_closed (
          self->skeleton_obj, previous_notification_id, reason);
      if (notification->close_task)
        g_task_return_boolean (notification->close_task, TRUE);
      g_ptr_array_remove (self->notification_list, notification);
      g_clear_pointer ((ClutterActor **)&self->window,
                       clutter_actor_destroy);
      if (self->notification_list->len > 0)
        {
          MildenhallNotification *next_notification = NULL;

          next_notification = g_ptr_array_index (self->notification_list,0);
          show_new_notification (next_notification);
        }
    }
}

static void
notification_window_action_invoked_cb (MildenhallNotificationWindow *actor,
                                       const gchar* button_name,
                                       gpointer user_data)
{
  guint id;
  MildenhallNotificationManager *self = user_data;

  id = mildenhall_notification_window_get_id (self->window);
  org_freedesktop_notifications_emit_action_invoked (self->skeleton_obj, id,
                                                     button_name);
}

static void
show_new_notification (MildenhallNotification *notification)
{
  MetaDisplay *display = NULL;
  ClutterActor *stage = NULL;
  GError *error = NULL;
  MildenhallNotificationManager *self = NULL;

  g_return_if_fail (notification != NULL);
  self = MILDENHALL_NOTIFICATION_MANAGER (notification->nm_ref);
  display = meta_plugin_get_display (self->plugin);
  stage = meta_get_stage_for_display (display);
  g_clear_pointer ((ClutterActor **)&self->window, clutter_actor_destroy);
  self->window = mildenhall_notification_window_new (notification, &error);
  if (error)
    {
      g_dbus_method_invocation_take_error (notification->notify_invocation,
                                           error);
      g_ptr_array_remove (self->notification_list, notification);
      if (self->notification_list->len > 0)
        {
          MildenhallNotification *next_notification = NULL;

          next_notification = g_ptr_array_index (self->notification_list,
                                                 0);
          show_new_notification (next_notification);
        }
      return;
    }
  g_signal_connect (self->window, "notify::state",
                    G_CALLBACK (notification_window_state_change_cb), notification);
  g_signal_connect (self->window, "action-invoked",
                    G_CALLBACK (notification_window_action_invoked_cb), self);
  clutter_actor_add_child (stage, CLUTTER_ACTOR (self->window));
  mildenhall_notification_window_show (self->window);
}

static void
mildenhall_notification_manager_get_property (GObject *object,
                                              guint property_id, GValue *value,
                                              GParamSpec *pspec)
{
  MildenhallNotificationManager *self = MILDENHALL_NOTIFICATION_MANAGER (object);

  switch (property_id)
    {
    case PROP_META_PLUGIN:
      g_value_set_object (value, self->plugin);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_notification_manager_set_property (GObject *object,
                                              guint property_id,
                                              const GValue *value,
                                              GParamSpec *pspec)
{
  MildenhallNotificationManager *self = MILDENHALL_NOTIFICATION_MANAGER (object);

  switch (property_id)
    {
    case PROP_META_PLUGIN:
      /*construct only */
      g_assert (self->plugin == NULL);
      g_set_object (&self->plugin, g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_notification_manager_dispose (GObject *object)
{
  MildenhallNotificationManager *self = MILDENHALL_NOTIFICATION_MANAGER (object);

  g_clear_pointer (&self->notification_list, g_ptr_array_unref);
  g_clear_object (&self->skeleton_obj);
  g_clear_object (&self->plugin);
  g_clear_pointer ((ClutterActor **)&self->window, clutter_actor_destroy);
  if (self->owner_id)
    {
      g_bus_unown_name (self->owner_id);
      self->owner_id = 0;
    }
  G_OBJECT_CLASS (mildenhall_notification_manager_parent_class)->dispose (
      object);
}

static void
mildenhall_notification_manager_class_init (MildenhallNotificationManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = mildenhall_notification_manager_get_property;
  object_class->set_property = mildenhall_notification_manager_set_property;
  object_class->dispose = mildenhall_notification_manager_dispose;

  /**
   * MildenhallNotificationManager:meta-plugin:
   *
   * Meta plugin object to hold a reference for making MetaPlugin calls.
   *
   * Since: 0.4.0
   */
  pspec = g_param_spec_object ("meta-plugin", "Meta Plugin",
                               "Meta plugin object to hold a reference for making MetaPlugin calls.",
                               META_TYPE_PLUGIN,
                               G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                               G_PARAM_STATIC_STRINGS);
  g_object_class_install_property (object_class, PROP_META_PLUGIN, pspec);
}

static void
mildenhall_notification_manager_init (MildenhallNotificationManager *self)
{
  self->notification_list = g_ptr_array_new_with_free_func (
       (GDestroyNotify) mildenhall_notification_free);
  self->state = MILDENHALL_NOTIFICATION_NONE;
  self->notification_id = 0;
  self->plugin = NULL;
  self->owner_id = g_bus_own_name (G_BUS_TYPE_SESSION,
                                   "org.freedesktop.Notifications", G_BUS_NAME_OWNER_FLAGS_NONE,
                                   on_bus_acquired, on_name_acquired,
                                   on_name_lost, self,
                                   NULL);
}

MildenhallNotificationManager *
mildenhall_notification_manager_new (MetaPlugin *plugin)
{
  return g_object_new (MILDENHALL_TYPE_NOTIFICATION_MANAGER,
                       "meta-plugin", plugin,
                       NULL);
}
