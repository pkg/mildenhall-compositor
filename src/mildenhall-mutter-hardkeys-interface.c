/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-mutter-plugin.h"

extern guint  mildenhall_plugin_debug_flags;
void
app_mgr_hk_name_appeared (GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data);

void
app_mgr_hk_name_vanished( GDBusConnection *connection,
                          const gchar     *name,
                          gpointer         user_data);

void
app_mgr_hk_proxy_clb( GObject *source_object,
                      GAsyncResult *res,
                      gpointer user_data);

void init_hardkey_client_handler( MetaPlugin *plugin )
{
  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Canterbury",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    app_mgr_hk_name_appeared,
                    app_mgr_hk_name_vanished,
                    plugin,
                    NULL);

}

void
app_mgr_hk_name_appeared (GDBusConnection *connection,
                          const gchar     *name,
                          const gchar     *name_owner,
                          gpointer         user_data)
{
  mildenhall_plugin_debug("app_mgr_hk_name_appeared\n");

  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_hard_keys_proxy_new (
                                          connection,
                                          G_DBUS_PROXY_FLAGS_NONE,
                                          "org.apertis.Canterbury",
                                          "/org/apertis/Canterbury/HardKeys",
                                          NULL,
                                          app_mgr_hk_proxy_clb,
                                          user_data);
}


void
app_mgr_hk_name_vanished( GDBusConnection *connection,
                          const gchar     *name,
                          gpointer         user_data)
{

  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("app_mgr_hk_name_vanished \n");
  if(NULL != priv->app_mgr_hk_proxy)
   g_object_unref(priv->app_mgr_hk_proxy);
}

void
app_mgr_hk_proxy_clb( GObject *source_object,
                      GAsyncResult *res,
                      gpointer user_data)
{
  GError *error;

  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (user_data)->priv;

  mildenhall_plugin_debug("app_mgr_hk_proxy_clb \n");
  /* finishes the proxy creation and gets the proxy ptr */
  priv->app_mgr_hk_proxy = canterbury_hard_keys_proxy_new_finish(res , &error);

  if(priv->app_mgr_hk_proxy == NULL)
  {
    g_printerr("error %s \n",g_dbus_error_get_remote_error(error));
    return;
  }


}

