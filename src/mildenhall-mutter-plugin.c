/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-mutter-plugin.h"
#include <cairo.h>
#include <stdlib.h>
#include <time.h>
#include <meta/util.h>
#include <meta/display.h>
#include <meta/meta-background-group.h>
#include <meta/meta-background-actor.h>
#include <meta/meta-background.h>

/*******************************************************************************
 * Fixes
 * Description                                  Date             Author
 *
 * 1) mutter background api added to	      	15-Jul-2014	Abhiruchi	 
 *    support version 3.10. mutter background, 
 *    overlay and gesture api updated.
******************************************************************************/

G_DEFINE_TYPE_WITH_PRIVATE (MildenhallPlugin, mildenhall_plugin, META_TYPE_PLUGIN)

#define ACTOR_DATA_KEY "MCCP-Default-actor-data"
#define MILDENHALL_LAUNCHER_APP_NAME    "Launcher"

static void on_backclicked (ClutterClickAction *action, 
                            ClutterActor *actor, 
                            gpointer user_data);
static void on_homeclicked (ClutterClickAction *action, 
                            ClutterActor *actor, 
                            gpointer user_data);
static void on_clicked (ClutterClickAction *action, 
                        ClutterActor *actor,
                        gpointer user_data, 
                        guint keyval, 
                        guint16 hardware_keycode);

static gboolean connect_capture_event (gpointer userdata);
static void v_system_gesture_create (MetaDisplay *display, MetaPlugin *plugin);


guint back_button_source_id = 0;
guint grab_timeout = 0;
static guint signals[LAST_SIGNAL] = { 0 };

static GQuark inDataQuark = 0;

static gboolean gesture_begin_cb ( ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin );
static gboolean gesture_progress_cb ( ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin);
static void gesture_end_cb (ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin);
static void gesture_cancel_cb (ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin);

static const MetaPluginInfo *p_meta_plugin_info (MetaPlugin *plugin);
static void v_map_handler(MetaPlugin *plugin,MetaWindowActor *actor);
static void v_window_minimize (MetaPlugin *plugin, MetaWindowActor *window_actor);
static void v_window_unminimize (MetaPlugin *plugin, MetaWindowActor *window_actor);
static void v_plugin_destroy (MetaPlugin *plugin, MetaWindowActor *window_actor);
static void v_kill_window_handlers (MetaPlugin *plugin,	MetaWindowActor *window_actor);
static void put_window_onscreen (MetaPlugin *plugin,MetaWindowActor *actor);


static void
mildenhall_plugin_dispose (GObject *object)
{
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (object)->priv;

  g_clear_object (&priv->notification_manager);
  G_OBJECT_CLASS (mildenhall_plugin_parent_class)->dispose (object);
}

static void
mildenhall_plugin_finalize (GObject *object)
{
	G_OBJECT_CLASS (mildenhall_plugin_parent_class)->finalize (object);
}

static void
mildenhall_plugin_set_property (GObject      *object,
		guint         prop_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	switch (prop_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}

static void
mildenhall_plugin_get_property (GObject    *object,
		guint       prop_id,
		GValue     *value,
		GParamSpec *pspec)
{
	switch (prop_id)
	{
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
		break;
	}
}
static void disconnect_gesture_handlers_function(ClutterActor *stage,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	if(priv->action)
	{
		if(priv->begin_handler)
		{
			g_signal_handler_disconnect (priv->action, priv->begin_handler);
			g_signal_handler_disconnect (priv->action, priv->end_handler);
			g_signal_handler_disconnect (priv->action, priv->progress_handler);
			g_signal_handler_disconnect (priv->action, priv->cancel_handler);
			priv->progress_handler = 0;
			priv->cancel_handler = 0;
			priv->begin_handler = 0;
			priv->end_handler = 0;

		}
	}
}

static void connect_gesture_handlers_function(ClutterActor *stage,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;

	priv->begin_handler = g_signal_connect (priv->action, "gesture-begin", G_CALLBACK (gesture_begin_cb), plugin);
	priv->end_handler = g_signal_connect (priv->action, "gesture-end", G_CALLBACK (gesture_end_cb), plugin);
	priv->progress_handler = g_signal_connect (priv->action, "gesture-progress", G_CALLBACK (gesture_progress_cb), plugin);
	priv->cancel_handler = g_signal_connect (priv->action, "gesture-cancel", G_CALLBACK (gesture_cancel_cb), plugin);
}

static void set_wm_background(MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	MetaDisplay *display;
	ClutterActor *background;
	MetaBackground *image;
	GFile *file;

	clutter_actor_destroy_all_children(priv->background_group);
	display = meta_plugin_get_display (plugin);

	image = meta_background_new(display);
	file = g_file_new_for_path(PKGDATADIR"/background.png");
	meta_background_set_file(image, file, G_DESKTOP_BACKGROUND_STYLE_NONE);
	g_object_unref (file);

	background = meta_background_actor_new (display, 0);
	meta_background_actor_set_background(META_BACKGROUND_ACTOR(background),
		image);
	g_object_unref(image);

	clutter_actor_add_child(priv->background_group, background);
}

static gboolean gesture_begin_cb ( ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin )
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	if(NULL != priv->action)
	{
		ClutterGestureAction *gesture_action = CLUTTER_GESTURE_ACTION (priv->action);
		priv->gestureTimeStamp = g_get_monotonic_time ();

		if(  priv->swipe_in_animate == TRUE || priv->launcher_max_animate == TRUE || priv->launcher_apriv == NULL )
		{
			return FALSE;
		}

		if((priv->launcher_apriv->is_maximized == TRUE) && (priv->launcher_apriv->is_minimized == FALSE))
		{
			return FALSE;
		}

		clutter_gesture_action_get_press_coords ( gesture_action,0,&priv->flPressXBegin,&priv->flPressYBegin);
		mildenhall_plugin_debug("gesture_begin_cb x %f y %f \n", priv->flPressXBegin , priv->flPressYBegin);
		priv->flDeltaXPrev = 0;
	}

	/* Only begin the gesture iff the initial press is within the current HMI
	 * bounds */
	return (priv->flPressXBegin <= 800.0 && priv->flPressYBegin <= 480.0);
}

static gboolean gesture_progress_cb ( ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;

	mildenhall_plugin_debug(" %s \n ",__FUNCTION__);
	priv->inThreshold = GESTURE_DIST_THRESHOLD;

	if( NULL != priv->action)
	{
		if((priv->launcher_apriv->is_maximized == FALSE) && (priv->launcher_apriv->is_minimized == TRUE))
		{
			gfloat motion_x, motion_y;
			gfloat delta_x;

			ClutterGestureAction *gesture_action = CLUTTER_GESTURE_ACTION (priv->action);
			//enGestureSwipeDirection hDirection =  GESTURE_SWIPE_DIRECTION_UNKNOWN, vDirection =  GESTURE_SWIPE_DIRECTION_UNKNOWN;
			clutter_gesture_action_get_motion_coords(gesture_action,0,&motion_x,&motion_y);

			if(priv->flMotionXPrev == 0.0)
			{
				delta_x = motion_x - priv->flPressXBegin;
			}
			else
			{
				delta_x = motion_x - priv->flMotionXPrev;
			}
			/* check if an applicattion is on top or if launcher is currently visible */
			if(NULL != priv->prev_win_apriv && NULL != priv->prev_win_apriv->actor)
			{
				gfloat ApplicationX =clutter_actor_get_x(priv->prev_win_apriv->actor);
				gfloat ApplicationMoveX = ApplicationX + delta_x;
				if( ApplicationMoveX >=APP_START_POINT && ApplicationMoveX <= APP_END_POINT )
				{
					priv->flMotionXPrev = motion_x;
					mildenhall_plugin_debug("delta_x in progress function = %f \n",delta_x);
					if(delta_x!=0.0)
					{
					   if( ( (((priv->flDeltaXPrev>0.0) && (delta_x<0.0)) || ((priv->flDeltaXPrev<0.0) && (delta_x>0.0))) && (abs(delta_x)>25.0) )
 					     ||
					     ( ((priv->flDeltaXPrev>0.0) && (delta_x>0.0)) || ((priv->flDeltaXPrev<0.0) && (delta_x<0.0) ) ) )
                       			   {
					   	  mildenhall_plugin_debug("start moving prev %f new %f \n", priv->flDeltaXPrev , delta_x);
					    	  clutter_actor_move_by(CLUTTER_ACTOR(priv->prev_win_apriv->actor),delta_x,0.0);
					    	  clutter_actor_move_by(CLUTTER_ACTOR(priv->launcher_apriv->actor),delta_x,0.0);
                      			   }
		                           priv->flDeltaXPrev = delta_x;
					}
				}
			}
		}
	}
	return TRUE;
}

static gboolean
connect_capture_event (gpointer userdata)
{
	MetaPlugin *plugin = userdata;
	MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (plugin));
	ClutterActor *stage = meta_get_stage_for_display (display);

	connect_gesture_handlers_function(stage,plugin);

	return FALSE;
}

static void gesture_end_cb (ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	gfloat ApplicationX =0.0;

	priv->flMotionXPrev = 0.0;

	if ((NULL != priv->action) && (priv->launcher_apriv != NULL))
	{
		MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (plugin));
		ClutterActor *stage = meta_get_stage_for_display (display);
		ClutterGestureAction *gesture_action = CLUTTER_GESTURE_ACTION (priv->action);

		gfloat flPressXEnd, flPressYEnd;
		gfloat delta_x, delta_y;
		clutter_gesture_action_get_release_coords ( gesture_action,0,&flPressXEnd,&flPressYEnd);

		delta_x = flPressXEnd - priv->flPressXBegin;
		delta_y = flPressYEnd - priv->flPressYBegin;
		mildenhall_plugin_debug(" ##### gesture_end_cb x %f y %f #### \n", delta_x, delta_y);

		if((priv->launcher_apriv->is_maximized == TRUE) && (priv->launcher_apriv->is_minimized == FALSE))
		{
			mildenhall_plugin_debug(" launcher is on top \n ");
			return;
		}

		if(priv->prev_win_apriv != NULL && priv->prev_win_apriv->actor != NULL)
		{
			ApplicationX =clutter_actor_get_x(priv->prev_win_apriv->actor);
		}
		else
		{
			g_warning("no appication is on top \n");
		}
		//if( abs(delta_x) > abs(delta_y))
	    	if(1)
	        {
			//its a horizontal swipe
			if( ( ( ( delta_x > 0 ) && (delta_x > GESTURE_DIST_THRESHOLD )) && ((g_get_monotonic_time () - priv->gestureTimeStamp) < MONO_TIMEOUT) )
			   || (ApplicationX > GESTURE_THRESHOLD) )
			{
				mildenhall_plugin_debug("gesture_end_cb: horizontal swipe ,\n");
				canterbury_hard_keys_call_home_press(priv->app_mgr_hk_proxy , NULL , NULL , NULL);
			}
			else
			{
			    disconnect_gesture_handlers_function(stage,plugin);
				clutter_actor_save_easing_state(priv->launcher_apriv->actor);
				clutter_actor_set_easing_mode (priv->launcher_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
				clutter_actor_set_easing_duration (priv->launcher_apriv->actor, MAP_TIMEOUT/2);
				clutter_actor_set_x (priv->launcher_apriv->actor, LAUNCHER_POS);
				clutter_actor_restore_easing_state (priv->launcher_apriv->actor);

				if(priv->prev_win_apriv != NULL && priv->prev_win_apriv->actor != NULL)
				{
					clutter_actor_save_easing_state(priv->prev_win_apriv->actor);
					clutter_actor_set_easing_mode (priv->prev_win_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
					clutter_actor_set_easing_duration (priv->prev_win_apriv->actor, MAP_TIMEOUT/2);
					clutter_actor_set_x (priv->prev_win_apriv->actor,APP_START_POINT);
					clutter_actor_restore_easing_state (priv->prev_win_apriv->actor);
				}
				mildenhall_plugin_debug(" it should move to home screen");
				//add gtimeout add after mapout time and call caputure event
				g_timeout_add(MAP_TIMEOUT,connect_capture_event,plugin);
			}
		}
	}
}

static void gesture_cancel_cb (ClutterGestureAction *action,ClutterActor *actor,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;

	mildenhall_plugin_debug(" gesture_cancel_cb \n");

	if (NULL != priv->action && priv->launcher_apriv != NULL)
	{
		MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (plugin));
		ClutterActor *stage = meta_get_stage_for_display (display);
		ClutterGestureAction *gesture_action = CLUTTER_GESTURE_ACTION (priv->action);

		gfloat flPressXEnd, flPressYEnd;
		gfloat delta_x, delta_y;
		clutter_gesture_action_get_release_coords ( gesture_action,0,&flPressXEnd,&flPressYEnd);

		delta_x = flPressXEnd - priv->flPressXBegin;
		delta_y = flPressYEnd - priv->flPressYBegin;
		mildenhall_plugin_debug("gesture_cancel_cb x %f y %f \n", delta_x, delta_y);

		if((priv->launcher_apriv->is_maximized == TRUE) && (priv->launcher_apriv->is_minimized == FALSE))
		{
			return;
		}
		if(delta_x > 0)
		{
			disconnect_gesture_handlers_function(stage,plugin);
			clutter_actor_save_easing_state(priv->launcher_apriv->actor);
			clutter_actor_set_easing_mode (priv->launcher_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
			clutter_actor_set_easing_duration (priv->launcher_apriv->actor, MAP_TIMEOUT/2);
			clutter_actor_set_x (priv->launcher_apriv->actor, LAUNCHER_POS);
			clutter_actor_restore_easing_state (priv->launcher_apriv->actor);

			if(priv->prev_win_apriv != NULL && priv->prev_win_apriv->actor != NULL)
			{
				clutter_actor_save_easing_state(priv->prev_win_apriv->actor);
				clutter_actor_set_easing_mode (priv->prev_win_apriv->actor, CLUTTER_EASE_OUT_CUBIC);
				clutter_actor_set_easing_duration (priv->prev_win_apriv->actor, MAP_TIMEOUT/2);
				clutter_actor_set_x (priv->prev_win_apriv->actor,APP_START_POINT);
				clutter_actor_restore_easing_state (priv->prev_win_apriv->actor);
			}

			g_timeout_add(MAP_TIMEOUT,connect_capture_event,plugin);
		}
		else
		{
			//TBD
		}
	}
}

static void gesture_settings_changed(GSettings *settings,gchar *key,MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (plugin));
	ClutterActor *stage = meta_get_stage_for_display (display);
	GSettings*  pSchema = g_settings_new("org.apertis.mildenhall-compositor");
	if(NULL != pSchema)
	{
		if(g_strcmp0(key,"touch-points") == 0)
		{
			guint uinNoOfTouchPoints = g_settings_get_uint(pSchema , "touch-points");
			if((uinNoOfTouchPoints > 0) && (priv->bGestureEnabled == FALSE) )
			{
				disconnect_gesture_handlers_function(stage,plugin);
				clutter_gesture_action_set_n_touch_points( CLUTTER_GESTURE_ACTION(priv->action) , uinNoOfTouchPoints );
				connect_gesture_handlers_function(stage,plugin);
				priv->bGestureEnabled = TRUE;
			}
			else if((uinNoOfTouchPoints == 0) && (priv->bGestureEnabled == TRUE) )
			{
				disconnect_gesture_handlers_function(stage,plugin);
				priv->bGestureEnabled = FALSE;
			}
		}
	}
}
static void
v_system_gesture_create (MetaDisplay *display, MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	GSettings*  pSchema = g_settings_new("org.apertis.mildenhall-compositor");
	gboolean bSystemGestureEnabled =  g_settings_get_boolean(pSchema , "enable-system-gesture");
	guint uinNoOfTouchPoints;

	mildenhall_plugin_debug("bSystemGestureEnabled %d \n", bSystemGestureEnabled);
	g_signal_connect (pSchema,"changed",G_CALLBACK (gesture_settings_changed), plugin);

	uinNoOfTouchPoints = g_settings_get_uint(pSchema , "touch-points");

	priv->bGestureEnabled = FALSE;

	if(uinNoOfTouchPoints > 0)
	{
		ClutterActor *stage = meta_get_stage_for_display (display);

		priv->action = clutter_gesture_action_new ();
		clutter_actor_add_action (stage, priv->action);
		clutter_gesture_action_set_n_touch_points( CLUTTER_GESTURE_ACTION(priv->action) , uinNoOfTouchPoints );
		connect_gesture_handlers_function(stage,plugin);

		priv->bGestureEnabled = TRUE;
	}
}


static void on_backclicked (ClutterClickAction *action, 
                            ClutterActor *actor, 
                            gpointer user_data)
{
	/* generate an F12 keypress */
	on_clicked( action, actor, user_data, 0xffc9, 96 );
}

static void on_homeclicked (ClutterClickAction *action, 
                            ClutterActor *actor, 
                            gpointer user_data)
{
	/* generate an F11 keypress */
	on_clicked( action, actor, user_data, 0xffc8, 95 );
}

static void on_clicked (ClutterClickAction *action, 
                        ClutterActor *actor,
                        gpointer user_data, 
                        guint keyval, 
                        guint16 hardware_keycode)
{
	MetaPlugin *plugin = user_data;
	MetaDisplay *display = meta_plugin_get_display (META_PLUGIN (plugin));
	ClutterActor *stage = meta_get_stage_for_display (display);
	ClutterDeviceManager *device_manager = clutter_device_manager_get_default ();
	ClutterInputDevice *keyboard = NULL;
	ClutterEvent *ev1 = clutter_event_new(CLUTTER_KEY_PRESS);
	ClutterEvent *ev2 = clutter_event_new(CLUTTER_KEY_RELEASE);
	const GSList *devlist;
	time_t current_time = time(NULL);

	/* choose the first slave keyboard device to use for injecting events */
	devlist = (GSList *) clutter_device_manager_peek_devices(device_manager);
	for (; devlist != NULL ; devlist = g_slist_next(devlist))
	{
		ClutterInputDevice *d = devlist->data;

		if(clutter_input_device_get_device_type(d) == CLUTTER_KEYBOARD_DEVICE &&
			clutter_input_device_get_device_mode(d) == CLUTTER_INPUT_MODE_SLAVE){
			keyboard = d;
			clutter_input_device_set_enabled(keyboard, TRUE);
			break;
		}
	}

	if(keyboard){
		ev1->key.type = CLUTTER_KEY_PRESS;
		ev1->key.device = keyboard;
		ev1->key.stage = CLUTTER_STAGE(stage);
		ev1->key.time = current_time;
		ev1->key.keyval = keyval;
		ev1->key.hardware_keycode = hardware_keycode;
	
		ev2->key.type = CLUTTER_KEY_RELEASE;
		ev2->key.device = keyboard;
		ev2->key.stage = CLUTTER_STAGE(stage);
		ev2->key.time = current_time;
		ev2->key.keyval = keyval;
		ev2->key.hardware_keycode = hardware_keycode;
	
		clutter_event_put(ev1);
		clutter_event_put(ev2);
	}

	clutter_event_free(ev1);
	clutter_event_free(ev2);

}

static void start (MetaPlugin *plugin)
{
	ClutterAction *backclicked;
	ClutterAction *homeclicked;
	ClutterActor *backbutton = NULL;
	ClutterActor *homebutton = NULL;
	ClutterActor *stage;
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	MetaDisplay *display;
	ClutterColor out_of_bounds_color = { 25, 25, 25, 255 };
	ClutterActor *bottom_rect;
	/*Dark grey colour  */
	ClutterColor bottom_rect_color = { 0x80, 0x80, 0x80 , 0xff};
	ClutterActor *overlay = NULL;
	ClutterColor    black = { 0, 0, 0 , 255};

	priv->launcher_apriv = NULL;
	priv->prev_win_apriv = NULL;
	priv->app_switch_complete = FALSE;
	priv->map_complete = TRUE;
	priv->shutdown = FALSE;
	priv->splash_screen = NULL;

   /*detect whether we should draw the bezel or not*/
	priv->draw_bezel = (g_getenv ("MILDENHALL_DRAW_BEZEL") != NULL);
	if(priv->draw_bezel){
		priv->bezel_offset_x = BEZEL_OFFSET_X;
		priv->bezel_offset_y = BEZEL_OFFSET_Y;
	} else {
		priv->bezel_offset_x = 0;
		priv->bezel_offset_y = 0;
	}

	display = meta_plugin_get_display (plugin);

	/*function to create the gesture action by taking settings values */
	v_system_gesture_create(display,plugin);

	 /* commented because in mutter-3.20 it is not available */
        //ClutterActor* overlay = meta_get_overlay_group_for_display(display);

        overlay = meta_get_top_window_group_for_display(display);
	priv->window_group = meta_get_window_group_for_display(display);

  /* Add the bottom rectangle at bottom of the stage on top of which the widgets are positioned */
  bottom_rect = clutter_actor_new ();
  clutter_actor_set_background_color (bottom_rect, &bottom_rect_color);
  clutter_actor_set_size (bottom_rect, ASSUMED_SCREEN_WIDTH,
                          ASSUMED_BOTTOM_OF_BUTTONS_HEIGHT);
  clutter_actor_set_position (bottom_rect, 0, ASSUMED_SCREEN_HEIGHT -
                              ASSUMED_BOTTOM_OF_BUTTONS_HEIGHT);
  clutter_actor_add_child (overlay, bottom_rect);

	/* FIXME: the Mildenhall UI design is done in terms of
	 * a specific pixel resolution, and various applications
	 * rely on everything outside that being offscreen.
	 * Configure the compositor to clip every window to that area. */
	clutter_actor_set_clip (priv->window_group, 0, 0,
		ASSUMED_SCREEN_WIDTH,
		ASSUMED_SCREEN_HEIGHT);
	stage = meta_get_stage_for_display (display);
	/* avoid the area outside the window group's clip region
	 * being distractingly bright */
	clutter_actor_set_background_color(stage, &out_of_bounds_color);

	priv->background_group = meta_background_group_new();
	clutter_actor_insert_child_below(priv->window_group,
		priv->background_group, NULL);

	priv->app_container = clutter_actor_new(); /* New container to hold app placeholder */
	clutter_actor_set_clip(priv->app_container, MINI_LAUNCHER_WIDTH, STATUS_BAR_HEIGHT, APPLICATION_WIDTH, APPLICATION_HEIGHT); /* Clip the container for reverse animation for back button click */
	/* handle screen background */
	set_wm_background(plugin);

	clutter_actor_add_child(priv->window_group,priv->app_container);
	//clutter_actor_add_child(overlay,priv->app_container);



	priv->idle_animation = p_idle_animation_new();

	priv->app_placeholder = p_ui_texture_from_file (PKGDATADIR"/normal-app-bkg.png");

	priv->back_button = p_ui_texture_from_file (PKGDATADIR"/wm-back-drawer.png");
	if(priv->draw_bezel){
		priv->bezel = p_ui_texture_from_file (PKGDATADIR"/mildenhall.png");
		backbutton = p_ui_texture_from_file (PKGDATADIR"/mildenhall-backbutton.png");
		homebutton = p_ui_texture_from_file (PKGDATADIR"/mildenhall-homebutton.png");
	}

	clutter_actor_hide(priv->app_placeholder);

	clutter_actor_add_child(priv->app_container,priv->app_placeholder);
	clutter_actor_add_child(overlay,priv->back_button);
	if(priv->draw_bezel){
		clutter_actor_add_child(overlay,priv->bezel);
		clutter_actor_add_child(overlay,backbutton);
		clutter_actor_add_child(overlay,homebutton);
	}

	clutter_actor_set_position (priv->back_button,
								6 + priv->bezel_offset_x,
								394 + priv->bezel_offset_y);
	clutter_actor_set_position (priv->app_container,
								priv->bezel_offset_x,
								priv->bezel_offset_y);
	if(priv->draw_bezel){
		clutter_actor_set_position (priv->bezel, 0, 0);
		clutter_actor_set_position (backbutton, 97, 544);
		clutter_actor_set_position (homebutton, 843, 543);

		clutter_actor_set_reactive (backbutton, TRUE);
		clutter_actor_set_reactive (homebutton, TRUE);
		backclicked = clutter_click_action_new ();
		homeclicked = clutter_click_action_new ();
		clutter_actor_add_action (backbutton, backclicked);
		clutter_actor_add_action (homebutton, homeclicked);
		g_signal_connect (backclicked, "clicked", G_CALLBACK (on_backclicked), plugin);
		g_signal_connect (homeclicked, "clicked", G_CALLBACK (on_homeclicked), plugin);
	}

	priv->display_off = clutter_actor_new();
	clutter_actor_set_background_color(priv->display_off, &black);
	clutter_actor_set_size(priv->display_off , 800 , 480);
	clutter_actor_add_child(overlay,priv->display_off);
	clutter_actor_hide(priv->display_off);

	priv->idle_animation = p_idle_animation_new();

	clutter_actor_set_position(priv->idle_animation,
								393 + priv->bezel_offset_x,
								160 + priv->bezel_offset_y);
	clutter_actor_add_child(overlay,priv->idle_animation);
	/* all services are required to own a bus before they export their interfaces */
	mildenhall_plugin_debug("g_bus_own_name \n" );

	g_bus_own_name ( G_BUS_TYPE_SESSION,           // bus type
			"org.apertis.Canterbury.Mutter.Plugin",         // interface name
			G_BUS_NAME_OWNER_FLAGS_NONE,  // bus own flag, can be used to take away the bus and give it to another service
			on_bus_acquired,              // callback invoked when the bus is acquired
			on_name_acquired,             // callback invoked when interface name is acquired
			on_name_lost,                 // callback invoked when name is lost to another service or other reason
			plugin,                       // user data
			NULL);                        // user data free func

	init_hardkey_client_handler(plugin);

	clutter_actor_show (meta_get_stage_for_display (display));
}

static void
mildenhall_plugin_class_init (MildenhallPluginClass *klass)
{
	GObjectClass      *gobject_class = G_OBJECT_CLASS (klass);
	MetaPluginClass *plugin_class  = META_PLUGIN_CLASS (klass);

	gobject_class->finalize        = mildenhall_plugin_finalize;
	gobject_class->dispose         = mildenhall_plugin_dispose;
	gobject_class->set_property    = mildenhall_plugin_set_property;
	gobject_class->get_property    = mildenhall_plugin_get_property;

	plugin_class->start            = start;
	plugin_class->map              = v_map_handler;
	plugin_class->minimize         = v_window_minimize;
	plugin_class->unminimize       = v_window_unminimize;
	plugin_class->destroy          = v_plugin_destroy;
	plugin_class->plugin_info      = p_meta_plugin_info;
	plugin_class->kill_window_effects   = v_kill_window_handlers;

	signals[SIGNAL_APP_SWITCH_COMPLETE] = g_signal_new(
			"app-switch-complete",
			G_OBJECT_CLASS_TYPE(klass),
			G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED,
			0,
			NULL,
			NULL,
			g_cclosure_marshal_VOID__STRING ,
			G_TYPE_NONE,
			0
	);
}

static void
mildenhall_plugin_init (MildenhallPlugin *self)
{
	MildenhallPluginPrivate *priv;

	self->priv = priv = MILDENHALL_PLUGIN_GET_PRIVATE (self);
	priv->MildenhallPluginObj	 = self;
	priv->info.name        = "MILDENHALL Window Manager Plugin";
	priv->info.version     = "0.1";
	priv->info.author      = "Mildenhall";
	priv->info.license     = "Mildenhall";
	priv->info.description = "This MILDENHALL plugin implementation.";

	priv->swipe_in_animate = FALSE;
	priv->launcher_max_animate = FALSE;
  priv->notification_manager = mildenhall_notification_manager_new (META_PLUGIN (self));
}

/*
 * Actor private data accessor
 */
static void
free_actor_private (gpointer data)
{
	if (G_LIKELY (data != NULL))
		g_slice_free (ActorPrivate, data);
}

ActorPrivate *
get_actor_private (MetaWindowActor *actor)
{
	ActorPrivate *priv = g_object_get_qdata (G_OBJECT (actor), inDataQuark);

	if (G_UNLIKELY (inDataQuark == 0))
		inDataQuark = g_quark_from_static_string (ACTOR_DATA_KEY);

	if (G_UNLIKELY (!priv))
	{
		priv = g_slice_new0 (ActorPrivate);

		g_object_set_qdata_full (G_OBJECT (actor),
				inDataQuark, priv,
				free_actor_private);
	}

	return priv;
}

/*
 * Simple minimize handler: it applies a scale effect (which must be reversed on
 * completion).
 */
static void
v_window_minimize (MetaPlugin *plugin, MetaWindowActor *window_actor)
{
	MetaWindowType type;
	MetaWindow *meta_window = meta_window_actor_get_meta_window (window_actor);
	ClutterActor *actor  = CLUTTER_ACTOR (window_actor);


	type = meta_window_get_window_type (meta_window);
	g_printerr("minimize current app");
	if (type == META_WINDOW_NORMAL)
	{
		MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
		ActorPrivate *apriv = get_actor_private (window_actor);

		if( ( priv->prev_win_apriv != NULL ) && (priv->prev_win_apriv->actor == actor) )
		{
			start_app_swipe_out_animation(plugin , actor );
		}

		apriv->is_maximized = FALSE;
	}
	else
		meta_plugin_minimize_completed (plugin, window_actor);
}

static void
window_destroyed_cb (ClutterActor *actor,
                     gpointer      user_data)
{
  MetaPlugin *plugin = META_PLUGIN (user_data);
  MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;

  /* The actor (window) has been destroyed externally while we were waiting for
   * transitions to complete on it. Clear our pointer to it, which
   * show_new_window() will use as a signal to bail. */
  if (priv->data != NULL && priv->data->actor == actor)
    priv->data->actor = NULL;
}

static void
put_window_onscreen (MetaPlugin *plugin, MetaWindowActor *window_actor)
{
	MildenhallPluginPrivate *priv   = MILDENHALL_PLUGIN (plugin)->priv;
	MetaWindowType type;
	ClutterActor *actor = CLUTTER_ACTOR (window_actor);
	MetaWindow *meta_window = meta_window_actor_get_meta_window (window_actor);
	const gchar *title = meta_window_get_title (meta_window);

	type = meta_window_get_window_type (meta_window);
	DEBUG ("mapping window \"%s\" of type %d", title, type);
	clutter_actor_set_pivot_point(actor , 0 , 0);


	if (type == META_WINDOW_NORMAL || type == META_WINDOW_DOCK)
	{
		DEBUG ("normal or dock window");

		/* check if the launched window is launcher, if so, jut show the window and store its properties */
		if (g_strcmp0 (title, MILDENHALL_LAUNCHER_APP_NAME) == 0)
		{
			mildenhall_plugin_debug("launcher window show\n");

			meta_window_move_resize_frame (meta_window, TRUE,
						priv->bezel_offset_x,
						priv->bezel_offset_y,
						LAUNCHER_WINDOW_WIDTH, ASSUMED_SCREEN_HEIGHT);
			priv->launcher_apriv = get_actor_private (window_actor);
			priv->launcher_apriv->is_maximized = TRUE;
			priv->launcher_apriv->is_minimized = FALSE;
			priv->launcher_apriv->actor = actor;
			priv->launcher_apriv->meta_window = meta_window;
			priv->launcher_apriv->meta_window_actor = window_actor;

			back_button_source_id = g_timeout_add_seconds(4, hide_back_button, plugin);

			//clutter_actor_set_size (actor, 800, 480);
			canterbury_mutter_plugin_emit_launcher_state(priv->mutter_dbus_object, 1);
			mildenhall_plugin_debug("launcher stats %d %d \n" , priv->launcher_apriv->is_maximized , priv->launcher_apriv->is_minimized );
		}
		else if (g_strcmp0 (title, "mildenhall_statusbar") == 0)
		{
			mildenhall_plugin_debug("status bar \n");

			meta_window_move_resize_frame (meta_window, TRUE,
						priv->bezel_offset_x,
						priv->bezel_offset_y,
						ASSUMED_SCREEN_WIDTH, ASSUMED_SCREEN_HEIGHT);
			meta_window_make_above(
				meta_window_actor_get_meta_window(window_actor));
		}
		else if (g_strcmp0 (title, "mildenhall-popup-layer") == 0)
		{
			mildenhall_plugin_debug("popup layer \n");

			meta_window_move_resize_frame (meta_window, TRUE,
						priv->bezel_offset_x,
						priv->bezel_offset_y,
						ASSUMED_SCREEN_WIDTH, ASSUMED_SCREEN_HEIGHT);
			meta_window_make_above (
				meta_window_actor_get_meta_window (window_actor));
		}
		else if (type == META_WINDOW_NORMAL)
		{
			EffectCompleteData *data = NULL;
			ActorPrivate *apriv = NULL;

			mildenhall_plugin_debug("app window\n");
			meta_window_move_resize_frame (meta_window, TRUE,
						MINI_LAUNCHER_WIDTH + priv->bezel_offset_x,
						priv->bezel_offset_y,
						APPLICATION_WIDTH, APPLICATION_HEIGHT);
			if (priv->win_name_to_hide != NULL && g_strcmp0 (title, priv->win_name_to_hide) == 0)
			{
				mildenhall_plugin_debug(" hide app window\n");
				clutter_actor_hide(actor);
				g_free(priv->win_name_to_hide);
				priv->win_name_to_hide = NULL;
				return;
			}
			else if(priv->launcher_apriv == NULL)
			{
				mildenhall_plugin_debug(" launcher is not ready\n");
				return;
			}

			data = g_new0 (EffectCompleteData, 1);
			apriv = get_actor_private (window_actor);
			data->actor = actor;
			data->plugin = plugin;
			apriv->meta_window = meta_window;
			apriv->meta_window_actor = window_actor;

			data->destroy_id = g_signal_connect (actor, "destroy",
			                                     (GCallback) window_destroyed_cb,
			                                     plugin);

			/* check if launcher is in maximized state , if so swipe in the new window, swipe out the launcher */

			priv->map_complete = FALSE;
			if(priv->app_switch_complete)
			{
				show_new_window(data);
			}
			else
			{
				mildenhall_plugin_debug("wait for app switch complete \n");
				priv->data = data;
			}

		}
		else
		{
			DEBUG ("unknown dock window");
		}
	}
}

static void
v_window_unminimize (MetaPlugin *plugin, MetaWindowActor *window_actor)
{
	put_window_onscreen(plugin,window_actor);
	meta_plugin_unminimize_completed(plugin, window_actor);
}

/*
 * Simple map handler: it applies a scale effect which must be reversed on
 * completion).
 */
static void
v_map_handler (MetaPlugin *plugin, MetaWindowActor *window_actor)
{
	put_window_onscreen(plugin,window_actor);
	meta_plugin_map_completed(plugin, window_actor);
}

static void
v_plugin_destroy (MetaPlugin *plugin, MetaWindowActor *window_actor)
{
	MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
	if(priv->shutdown)
	{
		meta_plugin_destroy_completed (plugin, window_actor);
	}
	else
	{
		MetaWindowType type;
		ClutterActor *actor = CLUTTER_ACTOR (window_actor);
		MetaWindow *meta_window = meta_window_actor_get_meta_window (window_actor);
		mildenhall_plugin_debug(" destroy \n");
		type = meta_window_get_window_type (meta_window);

		if (type == META_WINDOW_NORMAL)
		{
			meta_plugin_destroy_completed (plugin, window_actor);
			if( ( priv->prev_win_apriv != NULL ) && (priv->prev_win_apriv->actor == actor) )
			{
				g_printerr("actor on top just killed");
				start_launcher_maximize_animation(plugin);
			}
		}
		else
			meta_plugin_destroy_completed (plugin, window_actor);
	}
}

static void _v_mutter_plugin_timeline_stop_emit_completed(ClutterTimeline *time_line)
{
if(time_line)
{
	clutter_timeline_stop (time_line);
	g_signal_emit_by_name (time_line, "completed", NULL);
}
}

static void
v_kill_window_handlers (MetaPlugin      *plugin,
		MetaWindowActor *window_actor)
{
	ActorPrivate *apriv;
	MetaWindow *meta_window = meta_window_actor_get_meta_window (window_actor);
	mildenhall_plugin_debug("kill_window_effects for window %s \n", meta_window_get_title(meta_window));
	apriv = get_actor_private (window_actor);
 _v_mutter_plugin_timeline_stop_emit_completed(apriv->tml_minimize);
 _v_mutter_plugin_timeline_stop_emit_completed(apriv->tml_maximize);
 _v_mutter_plugin_timeline_stop_emit_completed(apriv->tml_map);
 _v_mutter_plugin_timeline_stop_emit_completed(apriv->tml_destroy);
}

static const MetaPluginInfo *
p_meta_plugin_info (MetaPlugin *plugin)
{
	MildenhallPluginPrivate *priv = MILDENHALL_PLUGIN (plugin)->priv;
	return &priv->info;
}

ClutterActor *p_ui_texture_from_file(const gchar* pFilePath)
{
	GFile *pFile = g_file_new_for_commandline_arg (pFilePath);
	gchar *pUri = NULL;
	ClutterActor *pBox = NULL;
	ClutterContent *pTexture = NULL;
	GdkPixbuf *pixbuf = NULL;
	GError *pErr = NULL;

	if (pFile == NULL)
	{
		g_warning ("%s: file Path is NULL\n", __FUNCTION__);
		return NULL;
	}

	pUri = g_file_get_path (pFile);
	pixbuf = gdk_pixbuf_new_from_file(pUri, &pErr);

	if (pixbuf == NULL || pErr != NULL)
	{
		g_warning ("%s\n", pErr->message);
		g_free (pUri);
		return NULL;
	}

	pTexture = clutter_image_new ();

	clutter_image_set_data(CLUTTER_IMAGE (pTexture),
			gdk_pixbuf_get_pixels (pixbuf),
			gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
					gdk_pixbuf_get_width (pixbuf),
					gdk_pixbuf_get_height (pixbuf),
					gdk_pixbuf_get_rowstride (pixbuf),
					&pErr);
	/* if error in setting image data, return */
	if (pErr != NULL)
	{
		g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, pErr->message);
		g_free (pUri);
		return NULL;
	}

	/* create the content box with image size and set content to it */
	pBox = clutter_actor_new ();
	clutter_actor_set_content (pBox, pTexture);
	clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));

	if(G_IS_OBJECT(pixbuf))
		g_object_unref (pixbuf);

	g_free (pUri);

	return pBox;
}

void
effect_complete_data_free (EffectCompleteData *data)
{
  g_return_if_fail (data != NULL);

  if (data->destroy_id != 0 && data->actor != NULL)
    g_signal_handler_disconnect (data->actor, data->destroy_id);

  g_free (data);
}
