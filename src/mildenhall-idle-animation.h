/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __IDLE_ANIMATION_H__
#define __IDLE_ANIMATION_H__

#include <glib-object.h>
#include <clutter/clutter.h>

G_BEGIN_DECLS

#define IDLE_TYPE_ANIMATION idle_animation_get_type()

#define IDLE_ANIMATION(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  IDLE_TYPE_ANIMATION, IdleAnimation))

#define IDLE_ANIMATION_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  IDLE_TYPE_ANIMATION, IdleAnimationClass))

#define IDLE_IS_ANIMATION(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  IDLE_TYPE_ANIMATION))

#define IDLE_IS_ANIMATION_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  IDLE_TYPE_ANIMATION))

#define IDLE_ANIMATION_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  IDLE_TYPE_ANIMATION, IdleAnimationClass))

typedef struct _IdleAnimation IdleAnimation;
typedef struct _IdleAnimationClass IdleAnimationClass;
typedef struct _IdleAnimationPrivate IdleAnimationPrivate;

struct _IdleAnimation
{
  ClutterActor parent;
  IdleAnimationPrivate *pPriv;
};

struct _IdleAnimationClass
{
  ClutterActorClass parent_class;
};

GType idle_animation_get_type (void) G_GNUC_CONST;

ClutterActor *p_idle_animation_new(void);
void v_idle_animation_start(ClutterActor *pIdleanimation);
void v_idle_animation_stop (ClutterActor *pIdleanimation);
void v_idle_animation_pause(ClutterActor *pIdleanimation);
void v_idle_animation_resume(ClutterActor *pIdleanimation);
void v_idle_animation_init_image_data(void);

G_END_DECLS

#endif /* __IDLE_ANIMATION_H__ */


