/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <meta/meta-plugin.h>
#include <meta/window.h>
#include <meta/theme.h>
#include <meta/display.h>
#include <meta/util.h>
#include <libintl.h>
#include <clutter/clutter.h>
#include <gmodule.h>
#include <string.h>
#include "mildenhall-idle-animation.h"
#include "canterbury.h"
#include "mildenhall-notification-manager.h"

#define DESTROY_TIMEOUT   250
#define MINIMIZE_TIMEOUT  250
#define MAXIMIZE_TIMEOUT  250
#define MAP_TIMEOUT       750
#define SWITCH_TIMEOUT    500
#define GESTURE_DIST_THRESHOLD 50

#define MONO_TIMEOUT 200000
#define GESTURE_THRESHOLD 400.0
#define APP_START_POINT 72
#define APP_END_POINT  872
#define LAUNCHER_POS -790

#define BEZEL_OFFSET_X 95
#define BEZEL_OFFSET_Y 64

/* FIXME: the Mildenhall UI design is done in terms of a specific pixel
 * resolution, and various applications rely on everything outside that
 * being offscreen. Preserve this until we have better
 * resolution-independence. */
#define ASSUMED_SCREEN_WIDTH             800
#define ASSUMED_SCREEN_HEIGHT            480
#define ASSUMED_BOTTOM_OF_BUTTONS_HEIGHT 17

/*
 * When a Mildenhall application is maximized, the minilauncher is
 * visible on the left.
 */
#define LAUNCHER_WINDOW_WIDTH            866

/*
 * Origin of the full launcher window when it is on the left
 * Showing only the minilauncher
 */
#define MINI_LAUNCHER_WIDTH             74
#define LAUNCHER_OUTSIDE_SCREEN_ORIGIN  (MINI_LAUNCHER_WIDTH-LAUNCHER_WINDOW_WIDTH)

#define STATUS_BAR_HEIGHT                50

/*
 * Room left for applications after removing launcher and status bar
 */
#define APPLICATION_WIDTH                (ASSUMED_SCREEN_WIDTH-MINI_LAUNCHER_WIDTH)
#define APPLICATION_HEIGHT               (ASSUMED_SCREEN_HEIGHT-STATUS_BAR_HEIGHT)

/*
 *  /----------------------------------------\  ^
 * /                                          \ |
 * |                                          | v bezel_height (Simulator)
 * |   |--|-------------------------------|   | ^                     ^
 * |   |  |                               |   | | STATUS_BAR_HEIGHT   |
 * |   |--|-------------------------------|   | v                     |
 * |   |  |                               |   | ^                     |
 * |   |--|                               |   | |          ASSUMED_SCREEN_HEIGHT
 * |   |--|                               |   | |                     |
 * |   |  |                               |   | APPLICATION_HEIGHT    |
 * |   |  |                               |   | |                     |
 * |   |  |                               |   | |                     |
 * |   |--|-------------------------------|   | v                     v
 * |                                          |
 * \                                          /
 *  \----------------------------------------/
 *     <--- ASSUMED_SCREEN_WIDTH --------->
 *     <--> MINI_LAUNCHER_WIDTH
 *         <---- APPLICATION_WIDTH ------->
 * <--> bezel_width (Simulator environment only)
*/

#define MILDENHALL_TYPE_PLUGIN                     (mildenhall_plugin_get_type ())
#define MILDENHALL_PLUGIN(obj)                     (G_TYPE_CHECK_INSTANCE_CAST ((obj), MILDENHALL_TYPE_PLUGIN, MildenhallPlugin))
#define MILDENHALL_PLUGIN_CLASS(klass)             (G_TYPE_CHECK_CLASS_CAST ((klass),  MILDENHALL_TYPE_PLUGIN, MildenhallPluginClass))
#define META_IS_DEFAULT_PLUGIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MILDENHALL_PLUGIN_TYPE))
#define META_IS_DEFAULT_PLUGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass),  MILDENHALL_TYPE_PLUGIN))
#define MILDENHALL_PLUGIN_GET_CLASS(obj)           (G_TYPE_INSTANCE_GET_CLASS ((obj),  MILDENHALL_TYPE_PLUGIN, MildenhallPluginClass))

#define MILDENHALL_PLUGIN_GET_PRIVATE(obj) \
		(G_TYPE_INSTANCE_GET_PRIVATE ((obj), MILDENHALL_TYPE_PLUGIN, MildenhallPluginPrivate))

typedef struct _MildenhallPlugin        MildenhallPlugin;
typedef struct _MildenhallPluginClass   MildenhallPluginClass;
typedef struct _MildenhallPluginPrivate MildenhallPluginPrivate;
typedef struct _ActorPrivate     ActorPrivate;
typedef enum _enGestureSwipeDirection enGestureSwipeDirection;

/* callback data for when animations complete */
typedef struct
{
	ClutterActor *actor;
	MetaPlugin *plugin;
	gulong destroy_id;  /* ID of the ClutterActor::destroy connection for @actor */
} EffectCompleteData;

void effect_complete_data_free (EffectCompleteData *data);

enum
{
	SIGNAL_APP_SWITCH_COMPLETE = 0,
	LAST_SIGNAL
};

/* gesture swipe direction on actor */
/**
 * enGestureSwipeDirection
 * @GESTURE_SWIPE_DIRECTION_UNKNOWN:
 * @GESTURE_SWIPE_DIRECTION_LEFT   : Leftwards swipe gesture
 * @GESTURE_SWIPE_DIRECTION_RIGHT  : Rightwards swipe gesture
 * @GESTURE_SWIPE_DIRECTION_UP     : Upwards swipe gesture
 * @GESTURE_SWIPE_DIRECTION_DOWN   : Downwards swipe gesture
 *
 * The main direction of the swipe gesture
 *
 */
enum _enGestureSwipeDirection
{
	GESTURE_SWIPE_DIRECTION_UNKNOWN = 0,
	GESTURE_SWIPE_DIRECTION_LEFT 	= 1 << 0,
	GESTURE_SWIPE_DIRECTION_RIGHT	= 1 << 1,
	GESTURE_SWIPE_DIRECTION_UP	    = 1 << 2,
	GESTURE_SWIPE_DIRECTION_DOWN	= 1 << 3,
};

struct _MildenhallPlugin
{
	MetaPlugin parent;

	MildenhallPluginPrivate *priv;
};

struct _MildenhallPluginClass
{
	MetaPluginClass parent_class;
};


/*
 * Plugin private data that we store in the .plugin_private member.
 */
struct _MildenhallPluginPrivate
{
	MetaPluginInfo         info;
	MildenhallPlugin 			*MildenhallPluginObj;
	ActorPrivate           *launcher_apriv;
	ActorPrivate           *prev_win_apriv;
	ClutterActor*             window_group;
	ClutterActor			*background_group;
	ClutterActor 			 *idle_animation;
	ClutterActor 			 *display_off;
	ClutterActor			 *app_placeholder;
	ClutterActor			 *navi_drawer;
	ClutterActor			 *global_search;
	ClutterActor			 *back_button;
	ClutterActor			 *bezel;
	ClutterActor			 *app_container;
	CanterburyMutterPlugin		 *mutter_dbus_object;
	EffectCompleteData 	 *data;
	gboolean               app_switch_complete;
	gboolean               map_complete;
	gboolean               shutdown;
	gchar*               win_name_to_hide;
	ClutterActor*        splash_screen;
	CanterburyHardKeys* app_mgr_hk_proxy;
	gchar*         app_type;
	ClutterAction *action;
	gfloat        flPressXBegin;
	gfloat        flPressYBegin;

	guint begin_handler;
	guint end_handler;
	guint progress_handler;
	guint cancel_handler;
	guint gestureTimeStamp;
	enGestureSwipeDirection hDirection;
	enGestureSwipeDirection vDirection;

	gboolean bGestureEnabled;

	gboolean swipe_in_animate;
	gboolean launcher_max_animate;

	gint inThreshold;
	gfloat flMotionXPrev;
        gfloat flDeltaXPrev;
  MildenhallNotificationManager *notification_manager;
	gboolean draw_bezel;
	guint bezel_offset_x;
	guint bezel_offset_y;

};

/*
 * Per actor private data we attach to each actor.
 */
struct _ActorPrivate
{
	ClutterActor *orig_parent;
	ClutterActor *actor;

	MetaWindow *meta_window;
	MetaWindowActor *meta_window_actor;

	ClutterTimeline *tml_minimize;
	ClutterTimeline *tml_maximize;
	ClutterTimeline *tml_destroy;
	ClutterTimeline *tml_map;

	gboolean      is_minimized ;
	gboolean      is_maximized ;
};


void
start_swipe_in_animation( MetaPlugin *plugin );

void
start_up_down_animation( MetaPlugin *plugin, gboolean back_click );

void
show_new_window (EffectCompleteData *data);

void
on_launcher_max_effect_complete (ClutterActor *actor,
		gpointer userdata);
void
start_launcher_maximize_animation( MetaPlugin *plugin );

void
start_app_swipe_out_animation( MetaPlugin *plugin  ,
		ClutterActor* actor );

ActorPrivate *get_actor_private (MetaWindowActor *actor);

ClutterActor *p_ui_texture_from_file(const gchar *pFile);

void
on_bus_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);

void
on_name_acquired (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);

void
on_name_lost (GDBusConnection *connection,
		const gchar     *name,
		gpointer         user_data);
void init_hardkey_client_handler( MetaPlugin *plugin );



gboolean
hide_back_button(gpointer userdata);

GType mildenhall_plugin_get_type (void) G_GNUC_CONST;

#define ERROR(format, ...) g_error ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define MESSAGE(format, ...) g_message ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define INFO(format, ...) g_info ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define mildenhall_plugin_debug(format, ...) DEBUG (format, ##__VA_ARGS__)
